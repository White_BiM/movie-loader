import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import Vuex from 'vuex';
import VueRouter from 'vue-router'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import MovieModal from '@/components/MovieModal'

Vue.config.productionTip = false
Vue.component('movie-modal', MovieModal)
Vue.use(Vuex, VueRouter, VueAxios, axios);
new Vue({
    vuetify,
    render: h => h(App),
    router,
    store,
    created() {
        this.$store.dispatch('getFilms')
        this.$store.dispatch('getGenres')
    }
}).$mount('#app')
