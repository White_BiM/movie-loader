/* eslint-disable no-console,no-unused-vars */
import axios from 'axios'
const ApiKey = '81a1cad5b8666724d11deba39397f9a1'
const HTTP = axios.create({
    baseURL:'https://api.themoviedb.org/3/',
})
export default {
    state: {
        loading: false,
        films: [],
        movie: [],
        moviesVideos: [],
        genres: []
    },
    mutations: {
        setLoading(state, payload) {
            state.loading = payload
        },
        setFilms(state, payload){
            state.films = payload.results
        },
        setMoviesData(state, payload){
            state.movie = payload
        },
        setMoviesVideos(state, payload){
            state.moviesVideos = payload.results
        },
        setGenres(state, payload){
            state.genres = payload.genres
        },
    },
    actions: {
        setLoading ({commit}, payload) {
            commit('setLoading', payload)
        },
        async getFilms({commit}){
            commit('setLoading', true)
            try{
                return HTTP.get(`discover/movie?sort_by=popularity.desc&api_key=${ApiKey}`).then(response => {
                    commit('setFilms', response.data)
                    commit('setLoading', false)
                })
            } catch(error){
                commit('setLoading', false)
                throw error
            }
        },
        async getMoviesData({commit}, id){
            try{
                 return HTTP.get(`movie/${id}?api_key=${ApiKey}`).then(response => {
                     commit('setMoviesData', response.data)
                 })
            } catch(error){
                throw error
            }
        },
        async getMoviesVideos({commit}, id){
            try{
                return HTTP.get(`movie/${id}/videos?api_key=${ApiKey}`).then(response => {
                    commit('setMoviesVideos', response.data)
                })
            } catch(error){
                throw error
            }
        },
        async getGenres({commit}){
            commit('setLoading', true)
            try{
                return HTTP.get(`genre/movie/list?language=en-US&api_key=${ApiKey}`).then(response => {
                    commit('setGenres', response.data)
                    commit('setLoading', false)
                })
            } catch(error){
                commit('setLoading', false)
                throw error
            }
        },
    async getMoviesByGenre({commit}, genre){
        commit('setLoading', true)
    try{
        return HTTP.get(`discover/movie?with_genres=${genre}&api_key=${ApiKey}`).then(response => {
            commit('setFilms', response.data)
            commit('setLoading', false)
        })
    } catch(error){
        commit('setLoading', false)
        throw error
    }
},
    },

    getters:  {
        loading(state) {
            return state.loading
        },
        films (state) {
            return state.films
        },
        movie (state) {
            return state.movie
        },
        moviesVideos(state) {
            return state.moviesVideos
        },
        genres (state) {
            return state.genres
        },
    }
}